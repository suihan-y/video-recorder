using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace VideoRecorder.Util;

public static class ImageUtils {
    /// <summary>
    /// Bitmap转为ImageSource
    /// </summary>
    /// <param name="bitmap"></param>
    /// <returns></returns>
    public static BitmapImage ToBitmapImage(this Bitmap bitmap) {
        using var memory = new MemoryStream();
        bitmap.Save(memory, ImageFormat.Bmp);
        memory.Position = 0;
        var bitmapimage = new BitmapImage();
        bitmapimage.BeginInit();
        bitmapimage.StreamSource = memory;
        bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
        bitmapimage.EndInit();
        return bitmapimage;
    }
}
