﻿using System.Diagnostics;

namespace VideoRecorder.Util;

/// <summary>
/// 时间间隔打印工具
/// </summary>
public class TimeLogger {
    private readonly Dictionary<string, DateTime?> _timeMap = new();

    /// <summary>
    /// 打印间隔时间
    /// </summary>
    /// <param name="id">标识</param>
    /// <param name="time">当前时间</param>
    public void Log(string id, DateTime? time = null) {
        var exists = _timeMap.TryGetValue(id, out var _prevTime);
        var now = time ?? DateTime.Now;
        if (!exists || _prevTime == null) {
            _timeMap[id] = now;
            return;
        }

        var ms = now.Subtract(_prevTime.Value).TotalMilliseconds;
        _timeMap[id] = now;
        var log = $"{id} => interval: {ms}ms";
        Debug.WriteLine(log);
        Console.WriteLine(log);
    }

    public void Clear() {
        _timeMap.Clear();
    }
}
