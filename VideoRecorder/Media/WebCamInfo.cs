﻿namespace VideoRecorder.Media;

/// <summary>
/// 摄像头信息
/// </summary>
public struct WebCamInfo {
    /// <summary>帧率</summary>
    public int Fps { get; set; }
    /// <summary>宽度</summary>
    public int Width { get; set; }
    /// <summary>高度</summary>
    public int Height { get; set; }

    public int BitCount { get; set; }


    public bool Equals(WebCamInfo other) {
        return Fps == other.Fps && Width == other.Width && Height == other.Height && BitCount == other.BitCount;
    }

    public override bool Equals(object obj) {
        return obj is WebCamInfo other && Equals(other);
    }

    public override int GetHashCode() {
        return HashCode.Combine(Fps, Width, Height, BitCount);
    }

    public override string ToString() {
        return $"{Width}x{Height}@{Fps}fps";
    }

    public static bool operator ==(WebCamInfo left, WebCamInfo right) {
        return left.Equals(right);
    }

    public static bool operator !=(WebCamInfo left, WebCamInfo right) {
        return !(left == right);
    }
}
